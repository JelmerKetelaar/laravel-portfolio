<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers;

//posts = index
Route::get('/', function () {
    return view('/index');
});

Route::get('/', 'ProductController@getProducts');
//
//Route::get('/cart', App\Http\Livewire\Cart::class);
//Route::get('/products', App\Http\Livewire\Product::class);

Route::get('/home', function () {
    return view('/home');
});

Route::get('/register', function () {
    return view('/register');
});

require __DIR__ . '/auth.php';
